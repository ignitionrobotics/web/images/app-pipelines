FROM markadams/chromium-xvfb

RUN apt-get clean
RUN apt-get update
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs git